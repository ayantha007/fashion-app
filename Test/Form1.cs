﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System.Json;
using fAppRepo;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                MongoClient dbClient = new MongoClient("mongodb+srv://user_1:12345@cluster0-cb81r.mongodb.net/test?retryWrites=true&w=majority");

                //Database List  
                var dbList = dbClient.ListDatabases().ToList();

                Console.WriteLine("The list of databases are :");
                foreach (var item in dbList)
                {
                    Console.WriteLine(item);
                }

                IMongoDatabase db = dbClient.GetDatabase("Test_1");
                var collList = db.ListCollections().ToList();
                Console.WriteLine("The list of collections are :");
                foreach (var item in collList)
                {
                    Console.WriteLine(item);
                }

                var things = db.GetCollection<BsonDocument>("testc");
              //  var things = db.GetCollection<BsonDocument>("testc");


                //CREATE  
                
                BsonDocument personDoc = new BsonDocument();
              //  personDoc.Add(new BsonElement("PersonAge", 23));

                Student s = new Student();
                s.Name = "aya";
                s.Id = "1";

                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(s);

                personDoc.Add(new BsonElement("json", jsonString));

                things.InsertOne(personDoc);

               

                //READ  
                var resultDoc = things.Find(new BsonDocument()).ToList();
                foreach (var item in resultDoc)
                {
                    Console.WriteLine(item.ToString());
                }


            }
            catch (Exception ex)
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //WardableItem

        }


    }

    class Student
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
