﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using fApp.DTO;
using fAppRepo;
using static fApp.DTO.Common;

namespace fApp.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WardrobeItemController : ControllerBase
    {

        private fAppRepo.Common _common = null;
        WardrobeItemController()
        {
            _common = new fAppRepo.Common();
        }


        [HttpGet()]
        [Route("users/{username}/wardrobe/items")]
        public async Task<ActionResult<RequestResult>> GetWarobeItems()
        {
            var result = await _common.GetList<WardableItem>("wardrobe");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully returned wardrobe items" },
                Data = new { data = result }
            });
        }


        [HttpGet()]
        [Route("users/{username}/wardrobe/item/{itemId}")]
        public async Task<ActionResult<bool>> GetWarobeItem(int itemId)
        {
            var result =  _common.Get<WardableItem>(itemId,"wardrobe");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully returned wardrobe items" },
                Data = new { data = result }
            });
        }


        [HttpPost()]
        [Route("users/{username}/wardrobe/items")]
        public async Task<ActionResult<bool>> PostWarobeItem(WardrobeItem wardrobeItem)
        {
            var result =  await _common.Save<fApp.DTO.WardrobeItem>(wardrobeItem, "wardrobe");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully created item" },
                Data = new { result = result }
            });
        }

        [HttpPut()]
        [Route("users/{username}/wardrobe/item/{itemId}")]
        public async Task<ActionResult<bool>> UpdateWarobeItem(WardrobeItem wardrobeItem,int itemId)
        {
            var result = await _common.Save<fApp.DTO.WardrobeItem>(wardrobeItem, "wardrobe");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully created item" },
                Data = new { result = result }
            });
        }




    }
}