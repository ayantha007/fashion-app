﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using fApp.DTO;
using static fApp.DTO.Common;

namespace fApp.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WardrobeExpertController : ControllerBase
    {
          

        private fAppRepo.Common _common = null;
        WardrobeExpertController()
        {
            _common = new fAppRepo.Common();
        }


        [HttpGet()]
        [Route("users/{username}/advices")]
        public async Task<ActionResult<RequestResult>> GetList()
        {
            var result = await _common.GetList<Advice>("wardrobe");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully returned wardrobe items" },
                Data = new { data = result }
            });
        }


        [HttpGet()]
        [Route("users/{username}/advices/{id}")]
        public async Task<ActionResult<bool>> Get(int itemId)
        {
            var result = _common.Get<Advice>(itemId, "wardrobe");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully returned wardrobe items" },
                Data = new { data = result }
            });
        }


        [HttpPost()]
        [Route("users/{username}/advices")]
        public async Task<ActionResult<bool>> Post(Advice item)
        {
            var result = await _common.Save<fApp.DTO.Advice>(item, "wardrobe");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully created item" },
                Data = new { result = result }
            });
        }

        [HttpPut()]
        [Route("users/{username}/advices/{id}")]
        public async Task<ActionResult<bool>> Update(Advice item, int itemId)
        {
            var result = await _common.Save<fApp.DTO.Advice>(item, "wardrobe");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully created item" },
                Data = new { result = result }
            });
        }


    }
}