﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using fApp.DTO;
using static fApp.DTO.Common;

namespace fApp.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {


        private fAppRepo.Common _common = null;
        UserController()
        {
            _common = new fAppRepo.Common();
        }


        [HttpGet()]
        [Route("users")]
        public async Task<ActionResult<RequestResult>> GetList()
        {
            var result = await _common.GetList<User>("user");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully returned users" },
                Data = new { data = result }
            });
        }


        [HttpGet()]
        [Route("users/{username}")]
        public async Task<ActionResult<bool>> Get(int itemId)
        {
            var result = _common.Get<User>(itemId, "user");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully returned wardrobe items" },
                Data = new { data = result }
            });
        }


        [HttpPost()]
        [Route("users/")]
        public async Task<ActionResult<bool>> Post(User item)
        {
            var result = await _common.Save<fApp.DTO.User>(item, "user");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully created item" },
                Data = new { result = result }
            });
        }

        [HttpPut()]
        [Route("users/{username}")]
        public async Task<ActionResult<bool>> Update(User item, int itemId)
        {
            var result = await _common.Save<fApp.DTO.User>(item, "user");

            return Ok(new RequestResult
            {
                Status = new Status { ResultCode = 12, Message = "Successfully created item" },
                Data = new { result = result }
            });
        }


    }
}