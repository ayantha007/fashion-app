﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fApp.DTO
{
    public class DesignItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string description { get; set; }
        public List<DesignItemImage> DesignItemImage { get; set; }


    }
}
