﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fApp.DTO
{
    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Type { get; set; }

    }
}
