﻿using System;

namespace fApp.DTO
{
    public class Common
    {
        public class RequestResult
        {
            public Status Status { get; set; }
            public Object Data { get; set; }
        }

        public struct Status
        {
            public int ResultCode { get; set; }
            public string Message { get; set; }
        }
    }
}
