﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fApp.DTO
{
    public class DesignItemImage
    {
        public Guid  Id { get; set; }
        public Guid  DesignId { get; set; }
        public string Name { get; set; }
    }
}
