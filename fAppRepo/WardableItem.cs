﻿using fApp.DTO;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace fAppRepo
{
    public class WardableItem :Common
    {

        public async Task<List<DesignItem>> GetItems()
        {
            List<DesignItem> items = new List<DesignItem>();

            //var documents =  base.Read("designItems");
            //IEnumerable <BsonElement> list = null;
            //if(documents!=null && documents.Count >0)

            //    list = documents[0].Elements;

            //foreach(BsonElement be in list)
            //{
               

            //}


            IMongoDatabase db = GetDataBase();

            var things = db.GetCollection<DesignItem>("DesignItem");
            items = await things.Find(new BsonDocument()).ToListAsync();

            return  items;
        }


        public async Task<DesignItem> GetItem(int id)
        {
            DesignItem items = null;
            //  return items;
            IMongoDatabase db = GetDataBase();
            var things = db.GetCollection<DesignItem>("DesignItem");
            var filter = Builders<DesignItem>.Filter.Eq("Id", id);
            var result =  things.Find(filter).Single();
            return result;
        }


        public async Task<bool> Save(DesignItem item)
        {

            //item.Id = Guid.NewGuid();
            //BsonDocument doc = new BsonDocument();
          
            //doc.Add(new BsonElement("Name", item.Name));
            //doc.Add(new BsonElement("Id", item.Id));
            //doc.Add(new BsonElement("Description", item.description));

            

            //base.Save(doc, "designItems");
           

            //BsonDocument docImages = new BsonDocument();
            //foreach (DesignItemImage image in item.DesignItemImage)
            //{
            //    docImages.Add(new BsonElement("Id", image.Id));
            //    docImages.Add(new BsonElement("DesignId", item.Id));
            //    docImages.Add(new BsonElement("Name", item.Name));
            //    base.Save(doc, "designitemImages");
            //}



           // MongoClient dbClient = new MongoClient("mongodb+srv://user_1:12345@cluster0-cb81r.mongodb.net/test?retryWrites=true&w=majority");


            IMongoDatabase db = GetDataBase();

            var things = db.GetCollection<DesignItem>("DesignItem");


            things.InsertOne(item);
            return true;
        }


        public async Task<bool> Update(DesignItem item,int id)
        {

            return true;
        }
    }
}
