﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace fAppRepo
{
    public class Common
    {

        public void SaveX(object obj,string collectionName)
        {
            MongoClient dbClient = new MongoClient("mongodb+srv://user_1:12345@cluster0-cb81r.mongodb.net/test?retryWrites=true&w=majority");


            IMongoDatabase db = dbClient.GetDatabase("Test_1");
           

            var things = db.GetCollection<BsonDocument>("testc");


            collectionName = "testc";
            //CREATE  
            BsonElement personFirstNameElement = new BsonElement("PersonFirstName", collectionName);

            BsonDocument personDoc = new BsonDocument();
            personDoc.Add(personFirstNameElement);
            personDoc.Add(new BsonElement("PersonAge", 23));

            things.InsertOne(personDoc);

        }


        public void SaveE(BsonDocument doc, string collectionName)
        {
            MongoClient dbClient = new MongoClient("mongodb+srv://user_1:12345@cluster0-cb81r.mongodb.net/test?retryWrites=true&w=majority");


            IMongoDatabase db = dbClient.GetDatabase("Test_1");

            var things = db.GetCollection<BsonDocument>(collectionName);   
            
           
            things.InsertOne(doc);

        }


        public IMongoDatabase GetDataBase()
        {
            MongoClient dbClient = new MongoClient("mongodb+srv://user_1:12345@cluster0-cb81r.mongodb.net/test?retryWrites=true&w=majority");


            IMongoDatabase db = dbClient.GetDatabase("Test_1");

            return db;
        }

        public async Task<bool> Save<T>(T type, string collectionName)
        {
            MongoClient dbClient = new MongoClient("mongodb+srv://user_1:12345@cluster0-cb81r.mongodb.net/test?retryWrites=true&w=majority");


            IMongoDatabase db = dbClient.GetDatabase("Test_1");

            var things = db.GetCollection<T>(collectionName);


            things.InsertOne(type);
            return true;
        }


        public async Task<List<T>> GetList<T>(string collectionName)
        {

            IMongoDatabase db = GetDataBase();

            var things = db.GetCollection<T>(collectionName);
            var items =  await things.Find(new BsonDocument()).ToListAsync();

            return items;
        }



        public T Get<T>(int Id, string collectionName)
        {

            IMongoDatabase db = GetDataBase();

            var things = db.GetCollection<T>(collectionName);
            var items = things.Find(new BsonDocument()).ToList();

            
            var filter = Builders<T>.Filter.Eq("Id", Id);
            var result = things.Find(filter).Single();
            return result;

           
        }


        public T Update<T>(int Id, string collectionName)
        {

            IMongoDatabase db = GetDataBase();

            var things = db.GetCollection<T>(collectionName);
            var items = things.Find(new BsonDocument()).ToList();


            var filter = Builders<T>.Filter.Eq("Id", Id);
            var result = things.Find(filter).Single();
            return result;


        }




        public List<BsonDocument> Read(string collectionName)
        {

            MongoClient dbClient = new MongoClient("mongodb+srv://user_1:12345@cluster0-cb81r.mongodb.net/test?retryWrites=true&w=majority");


            IMongoDatabase db = dbClient.GetDatabase("Test_1");

            var things = db.GetCollection<BsonDocument>(collectionName);

            var resultDoc = things.Find(new BsonDocument()).ToList();
            return resultDoc;
        }
    }
}
