﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fAppDomain
{
    public class Advice
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        List<AdviceItem> AdviceItem { get; set; }


    }
}
