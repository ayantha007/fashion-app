﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fAppDomain
{
    public class WardrobeItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
        public string Url { get; set; }
    }
}
