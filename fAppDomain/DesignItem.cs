﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fAppDomain
{
    public class DesignItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string description { get; set; }
        public List<DesignItemImage> DesignItemImage { get; set; }


    }
}
